#!/bin/bash
# Author: l.enright17@imperial.ac.uk
# Script: csvtospace.sh
# Desc: substitute the commas in the files with spaces
# saves the output into a .txt file
# Arguments: 1-> tab delimited file
# Date: Oct 2017
echo "Creating a space seperated version of $1 ..."
newname=$(tr -s ".csv" "-spaces" $1)
cat $1 | tr -s "," " " >> $newname.txt
echo "Done!"
exit
