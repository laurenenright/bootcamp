start=`date +%s%N | cut -b1-13`
Rscript Vectorize1.R >/dev/null
end=`date +%s%N | cut -b1-13`
time=$((end-start))

echo "The time taken to run Vectorize1.R is" $time "ms"

start=`date +%s%N | cut -b1-13`
python Vectorize1.py
end=`date +%s%N | cut -b1-13`
time=$((end-start))

echo "The time taken to run Vectorize1.py is" $time "ms"
echo ""


start=`date +%s%N | cut -b1-13`
Rscript Vectorize2.R
end=`date +%s%N | cut -b1-13`
time=$((end-start))

echo "The time taken to run Vectorize2.R is" $time "ms"


start=`date +%s%N | cut -b1-13`
python Vectorize2.py
end=`date +%s%N | cut -b1-13`
time=$((end-start))

echo "The time taken to run Vectorize2.py is" $time "ms"
