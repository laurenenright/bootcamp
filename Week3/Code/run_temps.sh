# runs TAutoCorr.R and then key_west_correlations.tex to produce a
# document detailing results of the p-test.

Rscript TAutoCorr.R # runs TAutoCorr.R to carry out calculations

# compile the document several times to avoid cross-referencing issues
latex key_west_correlations.tex
latex key_west_correlations.tex
latex key_west_correlations.tex

# delete unnecessary files
rm key_west_correlations.log
rm key_west_correlations.aux
