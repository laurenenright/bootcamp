#!/usr/bin/python

""" Sums elements to demonstrate speed of vectorising """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '17/10/2017' 

import scipy as sc 
import numpy as np
import pylab as p #Contains matplotlib for plotting

import sys

def SumAllElements(M):
	
	""" Manually sums the elements of a matrix M """
	
	Tot = 0
	
	for i in range(M.shape[0]):
		for j in range(M.shape[1]):
			Tot = Tot + M[i,j]

	return Tot


def main(argv):
	
	""" Main function for Vectorize1.py """
	
	M = np.random.uniform(0,1,(100,100))
	
	SumAllElements(M)
	
	return 0
	
if __name__ == "__main__":
	main(sys.argv)
