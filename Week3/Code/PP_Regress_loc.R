# This script uses the data:
##	EcolArchives-E089-51-D1.csv
# and produces ** files:
##	PP_Regress.pdf
##	PP_Regress_loc_Results.csv

# empty the workspace
rm(list=ls())

# open the lattice structure
library(ggplot2)
library(lattice)
library(ggthemes)

# read the data
MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv")
	
#### outputting linear regression results to csv ####

# find all the combinations of feeding types, predator lifestages and locations
combos <- data.frame(MyDF$Type.of.feeding.interaction,
					MyDF$Predator.lifestage,
					MyDF$Location)
combos <- as.matrix(unique(combos))
l_c <- length(combos[,1])

# preallocating a matrix to contain the results
output_mat <- matrix(rep(NA,9*l_c),l_c,9)

# an index
i <- 1
# cycling through feeding types
for (type in combos[,1])
{
	# the predator lifestage and location
	stage <- combos[i,2]
	loc <- combos[i,3]
	
	# extracting the subset from the dataframe
	new_DF <- subset(MyDF, Type.of.feeding.interaction==type 
							& Predator.lifestage==stage 
							& Location==loc)
	# how many samples are there in this analysis?
	no_samps <- length(new_DF[,1])
	
	# carry out the regression analysis
	suppressWarnings(my_lm <- summary(lm(Predator.mass ~ Prey.mass, data=new_DF)))
	
	# save data
	output_mat[i,1] <- type
	output_mat[i,2] <- stage
	output_mat[i,3] <- loc
	output_mat[i,4] <- no_samps
	if (length(my_lm$coefficients[,1])>1){ 
		# if there was enough data to find a regression slope
		output_mat[i,5] <- my_lm$coefficients[2,1]
	}
	output_mat[i,6] <- my_lm$coefficients[1,1]
	output_mat[i,7] <- my_lm$r.squared
	if (length(my_lm$fstatistic[1])>0){
		# if there was enough data to find an f statistic
		output_mat[i,8] <- my_lm$fstatistic[1]
	}
	output_mat[i,9] <- my_lm$coefficients[1,4]
	
	i=i+1
}

# write to dataframe
output_DF <- data.frame(output_mat)
colnames(output_DF) <- c("Type.of.feeding.interaction",
							"Predator.lifestage",
							"Location",
							"Number.of.samples",
							"Regression.slope",
							"Regression.intercept",
							"R.squared",
							"F.statistic.value",
							"p.value")

# output to csv
write.csv(output_DF, "../Results/PP_Regress_loc_Results.csv", row.names=FALSE) # write to file
