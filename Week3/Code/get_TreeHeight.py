#!/usr/bin/python

""" Takes an input file containing tree names, elevation angle of
	measurement, and distance of measurement from the tree, and outputs
	a file containing the tree heights."""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '16/10/2017'

import sys
import scipy as sc
import re
import math

def tree_height(angle,distance):
	
	""" Takes an angle of elevation in degrees and its adjacent side 
		length of the right angled triangle, and gives the opposite side
		length. """
	
	angle_radians=angle*math.pi/180. # converts angle to radians
	height=distance*math.tan(angle_radians) # computes height

	return height
		
def my_csv_reader(data):
	
	no_entries=data[0].count("\"")/2 # number of entries per csv line
	
	my_output = [0 for lines in data] # preallocate output list length
	
	k=0
	for each_line in data:
		my_row=re.split(r",",each_line)
		for j in range(len(my_row)):
			my_row[j]=re.sub(r"[\n\r\"\']","",my_row[j])
			if re.search(r"[^1234567890.]",my_row[j])==None:
				my_row[j]=float(my_row[j])
		my_output[k]=my_row
		k=k+1
		
	return my_output
	
def my_csv_writer(my_input):
	
	no_rows=len(my_input)
	no_entries=len(my_input[0])
	
	csv_list=[0 for i in range(no_rows)]
	
	for i in range(no_rows):
		csv_list[i]=str(my_input[i][0])
		for k in range(1,no_entries):
			csv_list[i]=csv_list[i]+","+str(my_input[i][k])
		csv_list[i]=csv_list[i]+"\r\n"
		
		print csv_list[i]
		
	return csv_list
		

def main(argv):

	myfilename=sys.argv[1] 
	# extracting the filename argument
	myfilepath="../Data/" + myfilename 
	# constructing the input filepath
	filenamenocsv=myfilename.replace(".csv","") 
	# removing the extension from the filename	
	destinationpath="../Results/" + filenamenocsv + "_treeheightspy.csv"
	#constructing the destination filepath
	
	g = open(myfilepath,'r') # opening the data file
	
	the_csv = g.readlines()
	tree_data = my_csv_reader(the_csv)
	
	#~ print tree_data
	
	new_column = [0 for lines in the_csv]
	
	new_column[0] = "Height.m"
	
	for k in range(1,len(new_column)):
		new_column[k] = tree_height(tree_data[k][2],tree_data[k][1])	
	
	for j in range(len(tree_data)):
		tree_data[j].append(new_column[j])
		
	new_csv=my_csv_writer(tree_data)
	
	print new_csv
	
	f = open(destinationpath,'w')
	
	f.writelines(new_csv)
	f.close()
	
	return 0

if __name__ == "__main__":
	main(sys.argv)


