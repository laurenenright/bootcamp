#!/usr/bin/python

""" Runs the discrete Ricker model """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '17/10/2017'

import scipy as sc
import pylab as p
import math as m
import sys

def Ricker(N0=1,r=1,K=10,generations=50):
	
	""" Takes the inputs
		N0 initial population number
		r growth rate
		K carrying capacity
		generations number of subsequent generations to carry out
		and returns the timeline [0,...,generations] and a vector N of
		the population number in each generation """
		
	N = sc.zeros(generations+1)
	
	timeline=range(generations+1)
	
	N[0] = N0
	
	for t in range(generations):
		N[t+1]  = N[t]*m.exp(r*(1-N[t]/K))
		
	return timeline, N
	
	
def main(argv):
	
	# default values
	N0=1
	r=1
	K=10
	gens=50
	
	if len(sys.argv)==5:
		N0 = float(sys.argv[2])
		r = float(sys.argv[3])
		K = float(sys.argv[4])
		gens = float(sys.argv[5])
	
	f1 = p.figure() # Open empty figure object

	t, N = Ricker(N0,r,K,gens) # calculate the population

	p.plot(t, N, 'g-') # Plot
	p.title('Ricker population dynamics; r=%f, K=%f' % (r,K))
	p.xlabel('Generation number')
	p.ylabel('Population')
	p.grid()
	p.show()
	
	return 0

if __name__ == "__main__":
	main(sys.argv)
	
	
