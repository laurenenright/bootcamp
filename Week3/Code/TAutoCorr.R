## This script computes the p-value for successive year correlation in
## annual mean temperature for a dataset.
##
## Please run the bash script 'run_temps.sh' in order to carry out this
## computation, and compile a PDF 'key_west_correlations.pdf'.

# load the data
tempdata <- load("../Data/KeyWestAnnualMeanTemperature.RData")

require(xtable) # a table package

#~ # examine the data
print(ats, include.rownames=FALSE)

# create a table to go into the LaTeX file
ats_formatted <- data.frame(ats[1:20,1],ats[1:20,2],ats[21:40,1],ats[21:40,2],ats[41:60,1],ats[41:60,2],ats[61:80,1],ats[61:80,2],ats[81:100,1],ats[81:100,2])
names(ats_formatted) <- c('Year','Temp','Year','Temp','Year','Temp','Year','Temp','Year','Temp')
print(xtable(ats_formatted, label="temp_table",caption="Mean annual temperature recorded in Key West, Florida."),file="my_table.tex", include.rownames=FALSE)


# setting up a plotting environment
mypar <- par(mfrow=c(1,2))
layout(matrix(c(1,1,2,2),2,2),widths=c(1,1), heights=c(1,1))

# plotting the data
pdf("temp_graph.pdf")
plot(ats$Year, ats$Temp, type="l", col="#00489C", ann=FALSE)
title(main="Key West annual mean temperature", xlab="Year", ylab="Temperature")
dev.off()

no_points <- length(ats$Temp) # compute the number of data points

####### Successive-year pairs #######

# create the matrix of successive-year pairs
pairs <- matrix(c(ats$Temp[1:(no_points-1)],ats$Temp[2:no_points]),no_points-1,2,byrow=FALSE)

# compute the successive-year correlation
succ_corr <- cor(pairs[,1],pairs[,2])

# display the correlation
print("The correlation of successive year pairs is:")
print(succ_corr)


####### Random pairs #######

# number of trials
no_trials <- 10000

# preallocate the vector of random-pair correlations
rand_corr <- rep(NA, no_trials)

for (i in 1:no_trials){
	
	# shuffle the data randomly
	data <- sample(ats$Temp)

	# compute the random-pair correlation
	rand_corr[i] <- cor(data[1:(no_points-1)], data[2:no_points])
	
}

# boxplot to compare the results
pdf("boxplot.pdf")
boxplot(rand_corr,succ_corr, names=c("Random permutation", "Successive year pairs"), horizontal=TRUE)
title(main="Correlation of temperature data", ylab="Correlation coefficient")
dev.off()
par(mypar)

# compute the fraction of trials in which the correlation was greater
# 	than the successive-year pair correlation
p_value <- sum(rand_corr>succ_corr) / no_trials

# display the p-value
print("The p-value for the temperatures is:")
print(format(p_value, scientific=FALSE))

# save the variables to text files, for use in LaTeX
cat(c(no_trials),sep="",file="temp_N.txt")
cat(c(p_value*no_trials),sep="",file="temp_Nc.txt")
cat(c(format(p_value, scientific=FALSE)),sep="",file="temp_p.txt")
cat(c(succ_corr),sep="",file="temp_c.txt")
cat(c(100*(1-p_value)),sep="", file="temp_d.txt")
