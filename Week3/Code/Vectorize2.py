#!/usr/bin/python

""" Carries out the discrete Ricker model """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '17/10/2017' 

import scipy as sc 
import numpy as np
import math as m
import pylab as p #Contains matplotlib for plotting

import sys

def stochrick(p0=np.random.uniform(.5,1.5,1000),r=1.2,K=1,sigma=0.2,numyears=100):
	
	""" Computes the discrete stochastic Ricker model populations with 
	the inputs
	p0 initial population (for any number of populations)
	r growth rate
	K carrying capacity
	sigma standard deviation for the Gaussian fluctuation in growth
	numyears number of subsequent years to carry out
	and outputs the timeseries and the resulting population matrix."""
	
	number_populations = len(p0)
	timeseries = range(0,numyears+1)
	
	# initialise
	N = sc.zeros((numyears+1,number_populations))
	N[0,:] = p0
	
	for pop in range(number_populations):
		for yr in range(numyears):
			N[yr+1,pop] = N[yr,pop]*m.exp(r*(1-N[yr,pop]/K)+np.random.normal(0,sigma))
	
	return timeseries, N
	
	
def main(argv):
	
	""" Main function for Vectorize2.py """
	
	stochrick()
	
	return 0
	
if __name__ == "__main__":
	main(sys.argv)
