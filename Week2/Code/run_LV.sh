#!/usr/bin/bash

""" Running some numerical Lotka-Volterra models """

#__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
#__version__ = '0.0.1'
#__date__ = '12/10/2017' 


ipython LV1.py

ipython LV2.py 15

%run -p ipython LV1.py
