#!/usr/bin/python

""" Gives some demonstrations of for loops in Python """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '09/10/2017'

# for loops in Python

for i in range(5):
	print i
	
my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list:
	print k
	
total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands:
	print total + S
	
# while loops in Python
z = 0
while z < 100:
	z = z + 1
	print (z)
	
b = True
while b:
	print "GERONIMO! infinite loop! ctrl+c to stop!"
	
