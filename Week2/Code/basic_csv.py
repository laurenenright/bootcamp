#!/usr/bin/python

""" Takes the file ../Sandbox/testcsv.csv which contains tuples of at
	least 5 elements and writes a file containing the first and fifth
	elements only onto the file ../Sandbox/bodymass.csv.
	Also prints the text "The species is" followed by each of the first
	elements."""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '9/10/2017'

import csv

#Read a file containing:
# 'Species', 'Infraorder', 'Family', 'Distribution', 'Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')

csvread = csv.reader(f)
temp=[]

for row in csvread:
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
	
f.close()

# write a file containing only species name and Body mass
f = open('../Sandbox/testcsv.csv','rb')
g = open('../Sandbox/bodymass.csv','wb')

csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
	print row
	csvwrite.writerow([row[0],row[4]])
	
f.close()
g.close()
