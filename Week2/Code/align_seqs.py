#!/usr/bin/python

""" Computing best alignment match scores for genetic sequences, using
	input of sequences from a given text file, and outputting the result
	to a another specified file.
	align_seqs.py inputsequencesfile outputsolutionsfile"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '10/10/2017'

import sys
 
def order_sequences(seq1,seq2):
	
	""" Takes two genetic sequences and outputs the longest sequence s1,
		the shortest sequence s2, and the lengths of s1 and s2.
		(s1,s2,l1,l2) = order_sequences('ACTA','GATCCAGT')
		s1, s2 are strings; l1, l2 are numbers."""
		
	# assign the longest sequence s1, and the shortest to s2
	# l1 is the length of the longest, l2 that of the shortest

	# computing the lengths of the sequences
	l1 = len(seq1)
	l2 = len(seq2)
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths
	return (s1,s2,l1,l2)



def calculate_score(s1, s2, l1, l2, startpoint):
    
    """ Computes the matching score based on a given startpoint for
		alignment, outputting the result as a number score.
		calculate_score(longest string, shortest string, length of 
		 longest string, length of shortest string, startpoint)"""
    
    score = 0
    for i in range(l2):
        if (i + startpoint) < l1:
            # if its matching the character
            if s1[i + startpoint] == s2[i]:
                score = score + 1

    return score


def best_match(seq1,seq2):
	
	""" Computes the best alignment for a pair of sequences, taking in
		the sequences as strings and outputting as a string of the form:
		answerstring =
		Best alignment:
		----CGTGGC
		AATGCGAATCGTT
		Best score:
		3
		"""
	
	(s1,s2,l1,l2)=order_sequences(seq1,seq2) # order the sequences

	# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_displacement = i
			my_best_score = z
	
	answerstring="Best alignment: \n" + my_best_align + s1 + "\n \n" + "Best score: " + str(my_best_score)
	
	return answerstring
	

def main(argv):
	
	""" Main function for align_seqs.py """
	
	g = open(sys.argv[1],'r') # opens the specified sequences input file
	
	# outputting the first two lines as the two sequences
	seq1=g.readline()
	seq2=g.readline()
	
	answerstring=best_match(seq1,seq2) # computes the solution
	
	f = open(sys.argv[2],'w') # opens the specified write file
	
	# writing the solution into the file
	f.write(answerstring)
	f.close()
	
	print answerstring
	return answerstring
	
if __name__ == "__main__":
	main(sys.argv)
