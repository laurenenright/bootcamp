#!/usr/bin/python

""" Some functions exemplifying the use of control statements, 
	and demonstrating the use of doctest to test results """

#docstrings are considered part of the running code (normal comments are
#stripped). Hence, you can access your docstrings at run time.
	 
__author__ = 'Samraat Pawar (s.pawar@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '11/10/2017'

import sys
import doctest

def even_or_odd(x=0): # if not specified, x should take value 0

	"""Find whether a number x is even or odd
	
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is Odd!'
	
	in case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""	
	
	if x % 2 == 0:
		return "%d is Even!" % x
	return "%d is Odd!" % x

#~ def main(argv):
	#~ # sys.exit("don't want to do this right now!"
	#~ print even_or_odd(22)
	#~ print even_or_odd(33)
	#~ return 0
	
#~ if (__name__ == "__main__"):
	#~ status = main(sys.argv)
	#~ sys.exit(status)
		
# This block ^ has been surpressed because we want to run doctest
# instead of running the main function.

doctest.testmod()
