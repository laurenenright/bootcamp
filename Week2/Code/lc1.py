#!/usr/bin/python

""" Creating lists of bird names and masses based on a set containing
	information on each species on bird """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '10/10/2017'


birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively.

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR, IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

#################
print "\n (1) Using list comprehensions \n"

# takes the right element from the tuple for each bird species in the list
latin_names=sorted([data[0] for data in birds])
common_names=sorted([data[1] for data in birds])
body_masses=sorted([data[2] for data in birds])

# displaying the lists to the user
print "The latin names are "
print latin_names
print "\n"
print "The common names are "
print common_names
print "\n"
print "The mean body masses are "
print body_masses

#################
print "\n \n (2) Using conventional loops \n"

# defining the empty lists for each list
latin_names=set()
common_names=set()
body_masses=set()

for k in birds: # cycling through the birds in the list
	# adding the latin names, common names, body masses to the 
	# appropriate lists
	latin_names.add(k[0])
	common_names.add(k[1])
	body_masses.add(k[2])
	
# sorting the lists into alphabetical order	
latin_names=sorted(latin_names)
common_names=sorted(common_names)
body_masses=sorted(body_masses)

# displaying the result to the user
print "The latin names are "
print latin_names
print "\n"
print "The common names are "
print common_names
print "\n"
print "The mean body masses are "
print body_masses

