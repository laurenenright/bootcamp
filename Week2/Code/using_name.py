#!/usr/bin/env python
# Filename: using_name.py

""" Demonstrates the use of __name__ == '__main__' to determine whether
	the function is being run on its own or whether it's being imported.
	"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '09/10/2017'

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
