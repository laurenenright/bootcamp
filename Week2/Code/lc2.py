#!/usr/bin/python

""" Creating lists of high and low rainfall based on embedded rainfall 
	data """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '10/10/2017'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

###################
# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.

print "\n (1) and (2) Using list comprehensions"
print "\n The list of events where the rainfall was greater than 100mm is \n"

# takes tuples where rainfall is greater than 100mm, then sorts descending by rainfall
highrain_events=[events for events in sorted(rainfall, key=lambda event: event[1]) if events[1]>100]
 
print highrain_events 
 
###################
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

print "\n \n The list of months where the rainfall was less than 50mm is \n"

# takes the first element of the tuple (month) when rainfall is less than 50mm, by sorting first with rainfall ascending
lowrain_months=[events[0] for events in sorted(rainfall, key=lambda event: event[1], reverse=True) if events[1]<50]

print lowrain_months

####################
# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

print "\n \n (3) Using conventional loops"

# defining the empty sets
highrain_events=set()
lowrain_months=set()

# creating lists ordered by rainfall amount
rainfall_ascend=sorted(rainfall, key=lambda event: event[1])

# cycling through the rainfall events
for i in range(len(rainfall)):
	if rainfall_ascend[i][1]>100: # checking if the event was high rainfall
		highrain_events.add(rainfall_ascend[i]) # if so, add event to list
	elif rainfall_ascend[i][1]<50: # if not high rainfall, check if low rainfall (both cannot be true)
		lowrain_months.add(rainfall_ascend[i][0]) # if so, add event to list

highrain_events=set(sorted(highrain_events, reverse=True))
lowrain_months=set(lowrain_months)

# displaying the results to the user
print "\n The list of events where the rainfall was greater than 100mm is \n"
print highrain_events
print "\n \n The list of months where the rainfall was less than 50mm is \n"
print lowrain_months


