#!/usr/bin/python

""" A set of functions to demonstrate profiling in ipython """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '12/10/2017'

def a_useless_function(x):
	y=0
	for i in xrange(100000000):
		y=y+i
	return 0
	
def a_less_useless_function(x):
	y=0
	for i in xrange(100000):
		y=y+i
	return 0
	
def some_function(x):
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0
	
some_function(1000)
