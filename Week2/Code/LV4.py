#!/usr/bin/python

""" Simulating the Lotka-Volterra discrete time model"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '12/10/2017' 

import scipy as sc
import pylab as p #Contains matplotlib for plotting

import sys

def LV_discrete(r,a,z,e,K,pop_R0,pop_C0,n):
	
	""" LV_log_ODE(r,a,z,e,K,R0,C0,t_series) carries out the discrete 
		time Lotka-Volterra model.
		The inputs are
		r resource growth rate
		a consumer search rate
		z consumer mortality rate
		e consumer production efficiency
		K carrying capacity
		R0 initial resource density
		C0 initial consumer density
		n number of time steps
		The outputs are
		(R,C) = (resource, consumer) populations at each point along
			the time series. """
	
	pop_R=sc.zeros(n+1) # set a zero vector for the population sizes
	pop_C=sc.zeros(n+1)
	
	pop_R[0]=pop_R0 # initialise
	pop_C[0]=pop_C0
	
	for j in range(1,n+1):
		R_old=pop_R[j-1]
		C_old=pop_C[j-1]
		pop_C[j]=C_old + e*a*R_old*C_old - z*C_old
		pop_R[j]=R_old + r*R_old*(1-R_old/K)-a*R_old*C_old
		
	return pop_R, pop_C


def main(argv):
	
	""" Main function for LV4.py """

	# Define parameters:
	
	# Default values:
	r=1.	# Resource growth rate
	a=0.1	# Consumer search rate (determines consumption rate) 
	z=1.5	# Consumer mortality rate
	e=0.75	# Consumer production efficiency
	K=30	# Arbitrary scaling variable (carrying capacity)
	n=60	# Number of simulation steps
	
	
	if len(sys.argv)==2: # if only number of steps specified
		n=float(sys.argv[1])
		
	elif len(sys.argv)==5: # if only parameters specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		
	elif len(sys.argv)==6: # if parameters and number of steps specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		n=float(sys.argv[5])
	
	# Now define the time series -- n points:
	t = sc.arange(n+1)
	
	# initials conditions: 10 prey and 5 predators per unit area
	x0 = 10
	y0 = 5 
	
	prey, predators=LV_discrete(r,a,z,e,K,x0,y0,n)
	
	f1 = p.figure() # Open empty figure object
	
	p.plot(t, prey, 'g-', label='Resource density') # Plot
	p.plot(t, predators  , 'b-', label='Consumer density')
	p.grid()
	p.legend(loc='best')
	p.xlabel('Time step t')
	p.ylabel('Population')
	p.title('Consumer-Resource population dynamics \nr = %f, a = %f, z = %f, e = %f, K = %f'%(r,a,z,e,K))
	p.show()
	f1.savefig('../Results/prey_and_predators_discrete.pdf') #Save figure
	
	return prey, predators
	
if __name__ == "__main__":
	main(sys.argv)
