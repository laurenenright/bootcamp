#!/usr/bin/python

""" Some functions exemplifying the use of control statements"""

#docstrings are considered part of the running code (normal comments are
#stripped). Hence, you can access your docstrings at run time.
	 
__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '10/10/2017'

import sys

def even_or_odd(x=0): # if not specified, x should take value 0

	""" even_or_odd(x) returns "x is Even!" is x is even, and "x is Odd!"
		otherwise """
	if x % 2 == 0:
		return "%d is Even!" % x
	return "%d is Odd!" % x
	
	
def largest_divisor_five(x=120):
	
	""" largest_divisor_five(x) gives the largest divisor of x among 
		2,3,4,5, returning "No divisor found for x!" if none are, and
		"The largest divisor of x is y!" where y=2,3,4 or 5. """
		
	largest = 0
	if x % 5 == 0:
		largest = 5
	elif x % 4 ==0:
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: # When all other (if, elif) conditions are not met
		return "No divisor found for %d!" % x # Each function can return a value or a variable.
	return "The largest divisor of %d is %d" % (x, largest)
	
def is_prime (x=70):
	
	""" is_prime(x) finds whether x is prime, returning True or False,
		and outputting "x is a prime!" if it is, and 
		"x is not a prime: y is a divisor" if x is not prime, with y 
		being the smallest integer factor greater than 1. """
	
	for i in range (2,x): # "range" returns a sequence of integers
		if x % i == 0:
			print "%s is not a prime: %d is a divisor" % (x,i)
			return False
		print "%d is a prime!" % x
		return True
		
def find_all_primes(x=22):
	
	""" find_all_primes(x) returns how many primes there are between 2
		and x (inclusive), outputting "There are y primes between 2 and
		x"."""
	
	allprimes = []
	for i in range(2, x+1):
		if is_prime(i):
			allprimes.append(i)
	print "There are %d primes between 2 and %d" % (len(allprimes),x)
	return allprimes
	
def main(argv):
	
	""" The main function, demonstrating computations which use the
		other functions in this file. """
	
	print even_or_odd(22)
	print even_or_odd(33)
	print largest_divisor_five(120)
	print largest_divisor_five(121)
	print is_prime(60)
	print is_prime(59)
	print find_all_primes(100)
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
		
