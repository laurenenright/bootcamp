#!/usr/bin/python

""" Simulating the Lotka-Volterra discrete time model with Gaussian
	fluctuations in the resource growth rate"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '12/10/2017' 

import scipy as sc
import scipy.stats as st
import pylab as p #Contains matplotlib for plotting

import sys

def LV_fluct(r,a,z,e,K,pop_R0,pop_C0,n,sd):
	
	""" LV_log_ODE(r,a,z,e,K,R0,C0,t_series) carries out the discrete 
		time Lotka-Volterra model with Gaussian fluctuations in the
		resource growth rate.
		The inputs are
		r resource growth rate
		a consumer search rate
		z consumer mortality rate
		e consumer production efficiency
		K carrying capacity
		R0 initial resource density
		C0 initial consumer density
		n number of time steps
		sd standard deviation in resource growth fluctuation
		The outputs are
		(R,C) = (resource, consumer) populations at each point along
			the time series. """
	
	pop_R=sc.zeros(n+1) # set a zero vector for the population sizes
	pop_C=sc.zeros(n+1)
	
	growth_r=sc.zeros(n+1) # growth rates over time
	
	pop_R[0]=pop_R0 # initialise
	pop_C[0]=pop_C0
	growth_r[0]=r
	
	for j in range(1,n+1):
		var=st.norm.rvs(0,sd,size=1)[0] # Gaussian fluctuation
		
		R_old=pop_R[j-1]
		C_old=pop_C[j-1]
		pop_C[j]=C_old + e*a*R_old*C_old - z*C_old
		pop_R[j]=R_old + (r+var)*R_old*(1-R_old/K)-a*R_old*C_old
		
		growth_r[j]=r+var # store the growth rates over time
		
	return pop_R, pop_C, growth_r


def main(argv):
	
	""" Main function for LV5.py """

	# Define parameters:
	
	# Default values:
	r=1.	# Resource growth rate
	a=0.1	# Consumer search rate (determines consumption rate) 
	z=1.5	# Consumer mortality rate
	e=0.75	# Consumer production efficiency
	K=30	# Arbitrary scaling variable (carrying capacity)
	n=60	# Number of simulation steps
	sd=0.3
	
	if len(sys.argv)==2: # if only number of steps specified
		n=float(sys.argv[1])
		
	if len(sys.argv)==3: # if number of steps and st. dev specified
		n=float(sys.argv[1])
		sd=float(sys.argv[2])
		
	elif len(sys.argv)==5: # if only parameters specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		
	elif len(sys.argv)==6: # if parameters and steps specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		n=float(sys.argv[5])
		
	elif len(sys.argv)==7: # if parameters, steps and standard deviation specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		n=float(sys.argv[5])
		sd=float(sys.argv[6])	
	
	# Now define time series - at n points
	t = sc.arange(n+1)
	
	# initials conditions: 10 prey and 5 predators per unit area
	x0 = 10
	y0 = 5 
	
	prey, predators, growth = LV_fluct(r,a,z,e,K,x0,y0,n,sd)
	
	f1 = p.figure() # Open empty figure objects
	
	p.plot(t, prey, 'g-', label='Resource density') # Plot
	p.plot(t, predators  , 'b-', label='Consumer density')
	p.grid()
	p.legend(loc='best')
	p.xlabel('Time step t')
	p.ylabel('Population')
	p.title('Consumer-Resource population dynamics \n a = %f, z = %f, e = %f, K = %f, sd=%f'%(a,z,e,K,sd))
	p.show()
	f1.savefig('../Results/prey_and_predators_fluct.pdf') #Save figure
	
	f2 = p.figure()
	p.plot(t,growth, 'r-')
	p.grid()
	p.xlabel('Time step t')
	p.ylabel('Growth rate')
	p.title('Fluctuating resource growth rate')
	p.show()
	f2.savefig('../Results/prey_and_predators_fluct_rates.pdf')
	
	
	return prey, predators, growth
	
if __name__ == "__main__":
	main(sys.argv)
