#!/usr/bin/python

# How many times will 'hello' be printed?

""" Some functions exemplifying the use of control statements. The main
	function outputs some example calculations."""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk'
__version__ = '0.1.1'
__date__ = '09/10/17'

import sys

h=0
p=0

# 1)
for i in range(3,17):
	print 'hello'
	h=h+1
	
# 'hello' will be printed 14 times here (i=3,4,5,...,16)
p=p+14

# 2)
for j in range(12):
	if j % 3 ==0:
		print 'hello'
		h=h+1
		
# 'hello' will be printed 4 times here (i=0,3,6,9)
p=p+4

# 3)
for j in range(15):
	if j % 5 == 3:
		print 'hello'
		h=h+1
	elif j % 4 == 3:
		print 'hello'
		h=h+1
		
# 'hello' will be printed 5 times here (j=3,7,8,11,13)
p=p+5

# 4)
z = 0
while z != 15:
	print 'hello'
	h=h+1
	z = z + 3
	
# 'hello' will be printed 5 times here (z=0,3,6,9,12)
p=p+5

# 5)
z = 12
while z < 100:
	if z == 31:
		for k in range(7):
			print 'hello'
			h=h+1
	elif z == 18:
		print 'hello'
		h=h+1
	z = z + 1
	
# 'hello' will be printed 7+1 times here (z=31,k=0,1,...,6)+(z=18)
p=p+7+1

print "The prediction was %d and the actual number of times 'hello' was printed was %e" % (p,h)

def foo1(x):
	
	""" foo1(x) finds the square root of x and returns a floating point
		number """
	
	return x ** 0.5


	
def foo2(x,y):
	
	""" foo1(x,y) gives the maximum of (x,y) """
	
	if x > y:
		return x
	return y
	
 
	
def foo3(x,y,z):
	
	""" foo3(x,y,z) returns x, y, and z in ascending order in a list """
	
	if x>y:
		tmp=y
		y=x
		x=tmp
	if y>z:
		tmp=z
		z=y
		y=tmp
	return [x,y,z]
	


def foo4(x):
	
	""" foo4(x) returns x factorial using an iterative method """
	
	result = 1
	for i in range(1,x+1):
		result = result * i
	return result
	
	
	
	
# This is a recursive function, meaning that the function calls itself
def foo5(x):
	
	""" foo5(x) returns x factorial using a recursive method """
	
	if x==1:
		return 1
	return x * foo5(x-1)
	
	

def main(argv):
	
	""" This is the main function, demonstrating some calculations """
	
	print "The square root of 10 is %d" % foo1(10)
	print "The maximum of 8 and 4 is %d" % foo2(4,8)
	print "12,6,11.6 in ascending order is (%d,%e,%f)" % (foo3(12,6,11.6)[0],foo3(12,6,11.6)[1],foo3(12,6,11.6)[2])
	print "4 factorial is %d" % foo4(4)
	print "7 factorial is %d" % foo5(7)
	return 0

if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
