#!/usr/bin/python

""" Describes a function which contains a bug, in order to demonstrate
	debugging. """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '11/10/2017'


def createabug(x):
	
	""" A function which contains a bug. """
	
	y = x**4
	z = 0.
	y = y/z
	return y
	
createabug(25)
