#!/usr/bin/python

""" Simulating the Lotka-Volterra model using scipy integrate """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '12/10/2017' 

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

import sys

def LV2_dR_dt_defn(r,a,z,e,K):
	
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    def LV2_dR_dt(pops,t=0):
		R = pops[0]
		C = pops[1]
		dRdt = r*R*(1-R/K) - a*R*C
		dydt = -z*C + e*a*R*C
		return sc.array([dRdt, dydt])
	
	return LV2_dR_dt


def main(argv):
	
	""" Main function for LV2.py """

	#~ global r
	#~ global a
	#~ global z
	#~ global e
	#~ global K

	# Define parameters:
	
	# Default values:
	r=1.	# Resource growth rate
	a=0.1	# Consumer search rate (determines consumption rate) 
	z=1.5	# Consumer mortality rate
	e=0.75	# Consumer production efficiency
	K=50	# Arbitrary scaling variable (carrying capacity)
	t_end=25	# Time to end simulation
	
	if len(sys.argv)==2: # if only end time specified
		t_end=float(sys.argv[1])
		
	elif len(sys.argv)==5: # if only parameters specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		
	elif len(sys.argv)==6: # if parameters and endtime specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		t_end=float(sys.argv[5])
	
	# Now define time -- integrate from 0 to t_end, using at least 1000 points:
	t = sc.linspace(0, t_end,  1000*max(t_end,15)/15)
	
	# initials conditions: 10 prey and 5 predators per unit area
	x0 = 10
	y0 = 5 
	z0 = sc.array([x0, y0]) 

	pops, infodict = integrate.odeint(LV2_dR_dt_defn(r,a,z,e,K), z0, t, full_output=True)
	
	infodict['message']     # >>> 'Integration successful.'

	prey, predators = pops.T 
	# Sets the density of prey and predators for each time
	
	f1 = p.figure() # Open empty figure object
	
	p.plot(t, prey, 'g-', label='Resource density') # Plot
	p.plot(t, predators  , 'b-', label='Consumer density')
	p.grid()
	p.legend(loc='best')
	p.xlabel('Time')
	p.ylabel('Population')
	p.title('Consumer-Resource population dynamics; r=%f, a=%f, z=%f, e=%f' % (r,a,z,e))
	p.show()
	f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure
	
	return prey, predators
	
if __name__ == "__main__":
	main(sys.argv)
