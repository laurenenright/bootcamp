#!/usr/bin/python

""" Computing best alignment match scores for genetic sequences, using
	input of sequences from two given text files, and outputting the 
	result to a another specified file.
	align_seqs.py inputsequence1 inputsequence2 outputsolutionsfile"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '11/10/2017'

import sys

def get_sequences_fasta(input_file)

	""" get_sequences_fasta(x.fasta) extracts the genetic sequence from
		the fasta file x.fasta, by removing the first line and removing
		all new lines to return a single-line string. """

	# opening the specified sequence input files
	g = open(input_file,'r') 
	
	# removing the first line and removing new lines \n
	sequence=''join([line.strip() for line in g.readlines(1:)])
	
	return sequence
	
def best_match_long(seq1,name1,seq2,name2):
	
	""" Computes the best alignment for a pair of sequences, taking in
		the sequences as strings and outputting as a string of the form:
		answerstring =
		Your sequences:
		Human genome and E. coli
		Best alignment:
		E. coli is displaced by 18 places
		Best score:
		3
		"""
	
	(s1,s2,l1,l2)=order_sequences(seq1,seq2) # order the sequences
	
	if s1==seq1:
		n1=name1
		n2=name2
	else
		n1=name2
		n2=name1

	# now try to find the best match (highest score)
	my_best_align = None
	my_best_score = -1

	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_displacement = i
			my_best_score = z
	
	answerstring="Your sequences: \n" n1 " and " n2 "\n \nBest alignment: \n" + n2 + " is displaced by " + str(my_best_displacement) + " places \n \n" + "Best score: " + str(my_best_score)
	
	return answerstring

def main(argv):
	
	""" Main function for align_seqs_fasta.py """
	
	# extracting the sequences from the fasta files
	seq1=get_sequences_fasta(sys.argv[1])
	seq2=get_sequences_fasta(sys.argv[2])
	
	# need to extract the names from the filenames
	
	answerstring=best_match_long(seq1,name1,seq2,name2) # computes the solution
	
	f = open(sys.argv[3],'w') # opens the specified write file
	
	# writing the solution into the file
	f.write(answerstring)
	f.close()
	
	print answerstring
	return answerstring
	
if __name__ == "__main__":
	main(sys.argv)
