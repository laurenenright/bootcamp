#!/usr/bin/python

""" Printing bird data in an easy to read format """

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '10/10/2017'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
        )

# Birds is a tuple of tuples of length three: latin name, common name, mass.
# write a (short) script to print these on a separate line for each species
# Hints: use the "print" command! You can use list comprehensions!

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

print "\n The birds are:"

# going through the birds and printing their information
for b in birds:
	print "\n"
	print b[0]
	print b[1]
	print b[2]
