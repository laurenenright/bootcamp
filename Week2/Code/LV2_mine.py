#!/usr/bin/python

""" Simulating the Lotka-Volterra model using my own numerical method"""

__author__ = 'Lauren Enright (l.enright17@imperial.ac.uk)'
__version__ = '0.0.1'
__date__ = '12/10/2017' 

import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

import sys

def LV_log_ODE(r,a,z,e,K,pop_R0,pop_C0,t_series):
	
	""" LV_log_ODE(r,a,z,e,K,R0,C0,t_series) creates a numerical
		solution to the Lotka-Volterra model with resource logistic
		growth.
		The inputs are
		r resource growth rate
		a consumer search rate
		z consumer mortality rate
		e consumer production efficiency
		K carrying capacity
		R0 initial resource density
		C0 initial consumer density
		t_series time series, which must have fixed interval
		The outputs are
		(R,C) = (resource, consumer) populations at each point along
			the time series. """
	
	t_step = t_series[1]-t_series[0] # the time step size
	
	pop_R=sc.zeros(t_series.size) # set a zero vector for the population sizes
	pop_C=sc.zeros(t_series.size)
	
	pop_R[0]=pop_R0 # initialise
	pop_C[0]=pop_C0
	
	for j in range(1,t_series.size):
		R_old=pop_R[j-1]
		C_old=pop_C[j-1]
		pop_C[j]=C_old + t_step*(e*a*R_old*C_old - z*C_old)
		pop_R[j]=R_old + t_step*(r*R_old*(1-R_old/K)-a*R_old*C_old)
		
	return pop_R, pop_C


def main(argv):
	
	""" Main function for LV2_mine.py """

	# Define parameters:
	
	# Default values:
	r=1.	# Resource growth rate
	a=0.1	# Consumer search rate (determines consumption rate) 
	z=1.5	# Consumer mortality rate
	e=0.75	# Consumer production efficiency
	K=50	# Arbitrary scaling variable (carrying capacity)
	t_end=25	# Time to end simulation
	
	if len(sys.argv)==2: # if only end time specified
		t_end=float(sys.argv[1])
		
	elif len(sys.argv)==5: # if only parameters specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		
	elif len(sys.argv)==6: # if parameters and endtime specified
		r = float(sys.argv[1])
		a = float(sys.argv[2])
		z = float(sys.argv[3])
		e = float(sys.argv[4])	
		t_end=float(sys.argv[5])
	
	# Now define time -- integrate from 0 to t_end, using at least 1000 points:
	t = sc.linspace(0, t_end,  10000*max(t_end,15)/15)
	
	# initials conditions: 10 prey and 5 predators per unit area
	x0 = 10
	y0 = 5 
	#~ z0 = sc.array([x0, y0]) 
	
	prey, predators=LV_log_ODE(r,a,z,e,K,x0,y0,t)
	
	f1 = p.figure() # Open empty figure object
	
	p.plot(t, prey, 'g-', label='Resource density') # Plot
	p.plot(t, predators  , 'b-', label='Consumer density')
	p.grid()
	p.legend(loc='best')
	p.xlabel('Time')
	p.ylabel('Population')
	p.title('Consumer-Resource population dynamics \nr = %f, a = %f, z = %f, e = %f, K = %f'%(r,a,z,e,K))
	p.show()
	f1.savefig('../Results/prey_and_predators_mine.pdf') #Save figure
	
	return prey, predators
	
if __name__ == "__main__":
	main(sys.argv)
